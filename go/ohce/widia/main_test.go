package testgame_test

import (
	testgame "tddgame/ohce/widia"
	"testing"

	"github.com/guregu/null"
	"github.com/stretchr/testify/assert"
)

func TestXxx(t *testing.T) {
	t.Run("should return ¡Buenas noches when input time >= 20 and time < 6 hours and input name is ohce <name>", func(t *testing.T) {
		mockRequestName := "ohce widia"
		mockRequestTime := null.IntFrom(20)
		mockResponse := "¡Buenas noches widia"

		got := testgame.TestGame(mockRequestName, mockRequestTime)

		assert.Equal(t, mockResponse, got)
	})

	t.Run("should return ¡Buenos días when input time >= 6 and time < 12 hours and input name is ohce <name>", func(t *testing.T) {
		mockRequestName := "ohce widia"
		mockRequestTime := null.IntFrom(6)
		mockResponse := "¡Buenos días widia"

		got := testgame.TestGame(mockRequestName, mockRequestTime)

		assert.Equal(t, mockResponse, got)
	})

	t.Run("should return ¡Buenas tardes when input time >= 12 and time < 20 hours and input name is ohce <name>", func(t *testing.T) {
		mockRequestName := "ohce widia"
		mockRequestTime := null.IntFrom(12)
		mockResponse := "¡Buenas tardes widia"

		got := testgame.TestGame(mockRequestName, mockRequestTime)

		assert.Equal(t, mockResponse, got)
	})

	t.Run("should return reversed name when input name is not palindrome and input time is empty", func(t *testing.T) {
		mockRequestName := "widia"
		mockResponse := "aidiw"

		got := testgame.TestGame(mockRequestName, null.Int{})

		assert.Equal(t, mockResponse, got)
	})

	t.Run("should return reversed name and ¡Bonita palabra! when input name is palindrome and input time is not empty", func(t *testing.T) {
		mockRequestName := "malayalam"
		mockResponse := "malayalam"

		got := testgame.TestGame(mockRequestName, null.Int{})

		assert.Equal(t, mockResponse, got)
	})
}
