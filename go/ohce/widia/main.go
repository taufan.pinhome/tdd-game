package testgame

import (
	"fmt"

	"github.com/guregu/null"
)

func TestGame(name string, time null.Int) string {
	if time.Valid {
		switch true {
		case time.ValueOrZero() >= 20 && time.ValueOrZero() < 6:
			return fmt.Sprintf("¡Buenos noches %s", name)
		case time.ValueOrZero() >= 6 && time.ValueOrZero() < 12:
			return fmt.Sprintf("¡Buenos días %s", name)
		case time.ValueOrZero() >= 12 && time.ValueOrZero() < 20:
			return fmt.Sprintf("¡Buenos tardes %s", name)
		}
	}

	isPalindrome := isPalindrome(name)
	if isPalindrome {
		return fmt.Sprintf("¡Buenos tardes %s", name) // TODO: reverse name
	}

	return name // TODO: reverse name
}

func isPalindrome(str string) bool {
	for i := 0; i < len(str); i++ {
		j := len(str) - 1 - i
		if str[i] != str[j] {
			return false
		}
	}
	return true
}
