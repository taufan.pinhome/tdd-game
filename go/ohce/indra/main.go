package indra

import (
	"strings"
)

func calculateClock(clock int) string {
	if clock >= 6 && clock <= 12 {
		return "¡Buenos días"
	} else if clock > 12 && clock <= 20 {
		return "¡Buenas tardes"
	} else if clock <= 6 || clock >= 20 {
		return "¡Buenas noches"
	}
	return ""
}

func Ohce(greeting string, clock int) string {
	var result string
	words := strings.Fields(greeting)

	switch words[0] {
	case "ohce":
		result = calculateClock(clock)
		result = result + " " + words[1]
	case "Stop!":
		result = "Adios"
	}

	return result
}
