package indra_test

import (
	"tddgame/ohce/indra"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestXxx(t *testing.T) {
	t.Run("input ohce with name at 12 should return gretting", func(t *testing.T) {
		assert.Equal(t, "¡Buenos días indra", indra.Ohce("ohce indra", 12))
	})

	t.Run("input ohce with name at 13 should return gretting", func(t *testing.T) {
		assert.Equal(t, "¡Buenas tardes indra", indra.Ohce("ohce indra", 13))
	})

	t.Run("input ohce with name at 21 should return gretting", func(t *testing.T) {
		assert.Equal(t, "¡Buenas noches indra", indra.Ohce("ohce indra", 21))
	})

	t.Run("input ohce with name at 5 should return gretting", func(t *testing.T) {
		assert.Equal(t, "¡Buenas noches indra", indra.Ohce("ohce indra", 5))
	})

	t.Run("should stop", func(t *testing.T) {
		assert.Equal(t, "Adios", indra.Ohce("Stop!", 5))
	})
}
