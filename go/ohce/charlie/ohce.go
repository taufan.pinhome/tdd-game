package ohce

import (
	"strings"
	"time"
)

func Ohce(inputString string, now time.Time) string {
	var greeting string
	if now.Hour() >= 20 || now.Hour() < 6 {
		greeting = "¡Buenas noches"
	} else if now.Hour() >= 6 && now.Hour() < 12 {
		greeting = "¡Buenos días"
	} else if now.Hour() >= 12 && now.Hour() < 20 {
		greeting = "¡Buenas tardes"
	}

	if inputString != "" {
		return greeting + " " + inputString + "!"
	}

	return greeting
}

func IsPalindrome(inputString string) bool {
	inputString = strings.ReplaceAll(inputString, " ", "")
	inputString = strings.ToLower(inputString)
	for i := 0; i < len(inputString)/2; i++ {
		if inputString[i] != inputString[len(inputString)-1-i] {
			return false
		}
	}
	return true
}
