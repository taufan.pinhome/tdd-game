package ohce_test

import (
	ohce "tddgame/ohce/charlie"
	"testing"
	"time"
)

func TestOhce(t *testing.T) {
	type args struct {
		inputString string
		now         time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "inputted time, between 20 and 6 should, input empty return greetings ¡Buenas noches",
			args: args{
				inputString: "",
				now:         time.Date(2020, 1, 1, 21, 0, 0, 0, time.UTC),
			},
			want: "¡Buenas noches",
		},
		{
			name: "inputted time, between 20 and 6 should, input exist return greetings ¡Buenas noches <input>!",
			args: args{
				inputString: "input",
				now:         time.Date(2020, 1, 1, 21, 0, 0, 0, time.UTC),
			},
			want: "¡Buenas noches input!",
		},
		{
			name: "inputted time, between 6 and 12 should, input empty return greetings ¡Buenos días",
			args: args{
				inputString: "",
				now:         time.Date(2020, 1, 1, 7, 0, 0, 0, time.UTC),
			},
			want: "¡Buenos días",
		},
		{
			name: "inputted time, between 6 and 12 should, input exist return greetings ¡Buenos días <input>!",
			args: args{
				inputString: "input",
				now:         time.Date(2020, 1, 1, 7, 0, 0, 0, time.UTC),
			},
			want: "¡Buenos días input!",
		},
		{
			name: "inputted time, between 12 and 20 should, input empty return greetings ¡Buenas tardes",
			args: args{
				inputString: "",
				now:         time.Date(2020, 1, 1, 13, 0, 0, 0, time.UTC),
			},
			want: "¡Buenas tardes",
		},
		{
			name: "inputted time, between 12 and 20 should, input exist return greetings ¡Buenas tardes <input>!",
			args: args{
				inputString: "input",
				now:         time.Date(2020, 1, 1, 13, 0, 0, 0, time.UTC),
			},
			want: "¡Buenas tardes input!",
		},
		// todo: add test palindrome
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ohce.Ohce(tt.args.inputString, tt.args.now); got != tt.want {
				t.Errorf("Ohce() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_IsPalindrome(t *testing.T) {
	type args struct {
		inputString string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "inputted string is palindrome, return true",
			args: args{
				inputString: "oto",
			},
			want: true,
		},
		{
			name: "inputted string is not palindrome, return false",
			args: args{
				inputString: "oA",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ohce.IsPalindrome(tt.args.inputString); got != tt.want {
				t.Errorf("isPalindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}
