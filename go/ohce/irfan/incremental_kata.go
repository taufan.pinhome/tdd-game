package irfan

import (
	"fmt"
	"strconv"
	"strings"
)

func Add(numbers string) int {
	var result int
	var customDelimit string

	checkDelimitter := strings.SplitAfter(numbers, "//")
	if len(checkDelimitter) == 2 {
		customDelimit = strings.Split(checkDelimitter[1], "\n")[0]
	}

	if customDelimit != "" {
		strAfterDelimit := strings.SplitAfter(numbers, "\n")[1]
		splittedStr := strings.Split(strAfterDelimit, customDelimit)
		fmt.Println(splittedStr)
		for _, str := range splittedStr {
			number, _ := strconv.Atoi(str)
			result += ValidateNumber(number)

		}
	}

	splittedStr := strings.Split(numbers, ",")
	for _, str := range splittedStr {
		// comma delimit check
		number, err := strconv.Atoi(str)
		if err != nil {
			// new line delimit check
			splittedNewLines := strings.Split(str, "\n")
			for _, s := range splittedNewLines {
				number, _ = strconv.Atoi(s)
				result += ValidateNumber(number)
			}
		} else {
			result += ValidateNumber(number)
		}

	}

	return result
}

func ValidateNumber(number int) int {
	if number < 0 {
		fmt.Printf("negatives are not allowed, %d", number)
		return 0
	}
	if number > 1000 {
		return 0
	}

	return number
}
