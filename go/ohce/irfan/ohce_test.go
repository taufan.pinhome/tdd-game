package irfan

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestOhce(test *testing.T) {
	mockName := "irfan"
	mockOhce := fmt.Sprintf("ohce %s", mockName)
	test.Run("should return string as response", func(t *testing.T) {
		got := Ohce("hello", time.Now())

		assert.IsType(t, got, "string")
	})

	test.Run("should return `¡Buenas noches < your name >!` when time input is between 20:00 and 6:00", func(t *testing.T) {
		got := Ohce(mockOhce, time.Date(0, 0, 0, 21, 0, 0, 0, &time.Location{}))
		expected := fmt.Sprintf("¡Buenas noches %s!", mockName)

		assert.Equal(t, expected, got)
	})

	test.Run("should return `¡Buenos días < your name >!` when time input is between 6:00 and 12:00", func(t *testing.T) {
		got := Ohce(mockOhce, time.Date(0, 0, 0, 7, 0, 0, 0, &time.Location{}))
		expected := fmt.Sprintf("¡Buenos días %s!", mockName)

		assert.Equal(t, expected, got)
	})

	test.Run("should return `¡Buenas tardes < your name >!` when time input is between 12:00 and 20:00", func(t *testing.T) {
		got := Ohce(mockOhce, time.Date(0, 0, 0, 14, 0, 0, 0, &time.Location{}))
		expected := fmt.Sprintf("¡Buenas tardes %s!", mockName)

		assert.Equal(t, expected, got)
	})

}
