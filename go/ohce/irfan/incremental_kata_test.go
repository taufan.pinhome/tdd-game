package irfan

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAdd(test *testing.T) {
	test.Run("should return sum of two numbers when input is two numbers separated by comma", func(t *testing.T) {
		got := Add("1,2")

		assert.Equal(t, 3, got)
	})

	test.Run("should return 0 if input is empty string", func(t *testing.T) {
		got := Add("")

		assert.Equal(t, 0, got)
	})

	test.Run("should return sum of all numbers based on comma separated input", func(t *testing.T) {
		got := Add("1,2,3,4,5")

		assert.Equal(t, 15, got)
	})

	test.Run("should return sum of all numbers based on comma or new line separated input", func(t *testing.T) {
		got := Add("1,2\n3\n4,5")

		assert.Equal(t, 15, got)
	})

	test.Run("should return sum of all numbers based on a custom delimitter input and number input", func(t *testing.T) {
		got := Add("//;\n1;2;3;4;5;6")

		assert.Equal(t, 21, got)
	})

	test.Run("should return sum of all numbers based on input while excluding negative number", func(t *testing.T) {
		got := Add("1,2,3,-4,-5")

		assert.Equal(t, 6, got)
	})

	test.Run("should return sum of all numbers based on input while excluding number > 1000", func(t *testing.T) {
		got := Add("1,2,3,1001")

		assert.Equal(t, 6, got)
	})

	test.Run("should return sum of all numbers with any length of custom delimitter", func(t *testing.T) {
		got := Add("//***\n1***2***3")

		assert.Equal(t, 6, got)
	})
}
