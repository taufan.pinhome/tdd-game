package irfan

import (
	"fmt"
	"strings"
	"time"
)

func Ohce(inputString string, timeNow time.Time) (response string) {
	if strings.Contains(inputString, "ohce") {
		name := strings.Split(inputString, " ")[1]
		if (timeNow.Hour() > 20 && timeNow.Hour() <= 24) || (timeNow.Hour() >= 0 && timeNow.Hour() <= 6) {
			return fmt.Sprintf("¡Buenas noches %s!", name)
		}

		if timeNow.Hour() > 6 && timeNow.Hour() <= 12 {
			return fmt.Sprintf("¡Buenos días %s!", name)
		}

		if timeNow.Hour() > 12 && timeNow.Hour() <= 20 {
			return fmt.Sprintf("¡Buenas tardes %s!", name)
		}
	} else if CheckPalindrome(inputString) {

	}

	return response
}

func CheckPalindrome(input string) bool {

	return false
}
