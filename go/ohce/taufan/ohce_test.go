package taufan_test

import (
	"tddgame/ohce/taufan"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOhce(test *testing.T) {
	test.Run("should return ¡Buenas noches <input>! when called from 20 to before 6", func(t *testing.T) {
		assert.Equal(t, "¡Buenas noches taufan!", taufan.Ohce("taufan", 20))
		assert.Equal(t, "¡Buenas noches taufan!", taufan.Ohce("taufan", 5))
		assert.NotEqual(t, "¡Buenas noches taufan!", taufan.Ohce("taufan", 6))
	})

	test.Run("should return ¡Buenas días <input>! when called from 6 to before 12", func(t *testing.T) {
		assert.Equal(t, "¡Buenas días taufan!", taufan.Ohce("taufan", 6))
		assert.Equal(t, "¡Buenas días taufan!", taufan.Ohce("taufan", 11))
		assert.NotEqual(t, "¡Buenas días taufan!", taufan.Ohce("taufan", 12))
	})

	test.Run("should return ¡Buenas tardes <input>! when called from 12 to before 20", func(t *testing.T) {
		assert.Equal(t, "¡Buenas tardes taufan!", taufan.Ohce("taufan", 12))
		assert.Equal(t, "¡Buenas tardes taufan!", taufan.Ohce("taufan", 19))
		assert.NotEqual(t, "¡Buenas tardes taufan!", taufan.Ohce("taufan", 20))
	})

	test.Run("should return ¡Bonita palabra! regardless of time when input is palindrome", func(t *testing.T) {
		assert.Equal(t, "¡Bonita palabra!", taufan.Ohce("kasur rusak", 6))
		assert.Equal(t, "¡Bonita palabra!", taufan.Ohce("kasur rusak", 12))
		assert.Equal(t, "¡Bonita palabra!", taufan.Ohce("kasur rusak", 20))
	})

	test.Run("should handle hour exceeding 24 hours", func(t *testing.T) {
		assert.Equal(t, "¡Buenas noches taufan!", taufan.Ohce("taufan", 24))
		assert.Equal(t, "¡Buenas noches taufan!", taufan.Ohce("taufan", 29))
		assert.Equal(t, "¡Buenas días taufan!", taufan.Ohce("taufan", 30))
		assert.Equal(t, "¡Buenas tardes taufan!", taufan.Ohce("taufan", 43))
	})
}
