package taufan

import "fmt"

func Ohce(input string, currentHour int) string {
	if reverse(input) == input {
		return "¡Bonita palabra!"
	}

	if currentHour >= 24 {
		currentHour -= 24
	}

	switch true {
	case currentHour >= 20 || currentHour < 6:
		return fmt.Sprintf("¡Buenas noches %s!", input)
	case currentHour >= 6 && currentHour < 12:
		return fmt.Sprintf("¡Buenas días %s!", input)
	case currentHour >= 12 && currentHour < 20:
		return fmt.Sprintf("¡Buenas tardes %s!", input)
	}
	return ""
}

func reverse(s string) string {
	rns := []rune(s)
	for i, j := 0, len(rns)-1; i < j; i, j = i+1, j-1 {
		rns[i], rns[j] = rns[j], rns[i]
	}
	return string(rns)
}
