import { fizzbuzz } from "./fizzbuzz";

describe("fizzbuzz()", () => {
  it("should return 100 items", () => {
    const result: any[] = fizzbuzz();
    expect(result.length).toBe(100);
  });

  it("should return `fizz` for multiples of 3", () => {
    const result: any[] = fizzbuzz();
    for (let i = 2; i < result.length; i+=3) {
      const element = result[i];
      expect(element).toEqual("fizz");
    }
  });

  it("should return `buzz` for multiples of 5", () => {
    const result: any[] = fizzbuzz();
    for (let i = 2; i < result.length; i+=3) {
      const element = result[i];
      expect(element).toEqual("buzz");
    }
  });
});
