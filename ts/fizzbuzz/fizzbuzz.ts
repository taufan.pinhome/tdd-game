export function fizzbuzz(): any[] {
  return Array.from({length: 100}, (_, i) => {
    const iteration = i + 1;
    return iteration % 3 === 0 ? "fizz" : iteration;
  });
}
