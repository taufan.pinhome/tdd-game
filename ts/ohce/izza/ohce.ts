function checkPallindrom(text: string) {
  const check = text.length / 2;

  for (let i = 0; i < check; i++) {
    if (text[i] !== text[text.length - 1 - i]) return false;
  }

  return true;
}

export function ohce(message: string) {
  if (message === 'Stop!') {
    return 'Adios Pedro';
  }

  if (checkPallindrom(message)) return message;

  const now = new Date();

  if (now.getHours() < 20 && now.getHours() >= 12) {
    return `¡Buenas tardes ${message}!`;
  } else if (now.getHours() < 12 && now.getHours() >= 6) {
    return `¡Buenas días ${message}!`;
  }

  return `¡Buenas noches ${message}!`;
}