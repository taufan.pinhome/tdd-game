import { ohce } from "./ohce";

import MockDate from 'mockdate';

describe('Test ohce function based on my understanding', () => {
  afterEach(() => {
    MockDate.reset();
  });

  it('ohce return greeting correctly during 20-6', () => {
    MockDate.set('2018-11-28 23:28:30');
    expect(ohce('You')).toEqual('¡Buenas noches You!');
  });

  it('ohce return greeting correctly during 6-12', () => {
    MockDate.set('2018-11-28 6:28:30');
    expect(ohce('You')).toEqual('¡Buenas días You!')
  });

  it('ohce return greeting correctly during 12-20', () => {
    MockDate.set('2018-11-28 13:28:30');
    expect(ohce('You')).toEqual('¡Buenas tardes You!')
  });

  it('ohce return same input if input value pallindrom', () => {
    expect(ohce('kok')).toEqual('kok')
  });

  it('ohce return Adios Pedro if input is `Stop!`', () => {
    expect(ohce('Stop!')).toEqual('Adios Pedro')
  });
})