/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path");

module.exports = {
  clearMocks: true,
  coverageDirectory: "<rootDir>/coverage/unit",
  moduleFileExtensions: ["js", "json", "ts"],
  rootDir: path.resolve(__dirname, "."),
  setupFiles: [],
  testEnvironment: "node",
  testMatch: ["<rootDir>/**/*.spec.ts"],
  transform: {
    "^.+\\.(t|j)s$": "ts-jest",
  },
  verbose: true,
};
